import matplotlib.pyplot as plt
import numpy as np

pan = np.array([
    0x00, 0x01, 0x03, 0x07, 0x0D, 0x15, 0x1E, 0x29, 0x34, 0x42,
    0x51, 0x5E, 0x67, 0x6E, 0x73, 0x77, 0x7A, 0x7C, 0x7D, 0x7E,
    0x7F,
]) / 0x80

def f():
    def plot(exp):
        curve = pan ** exp
        plt.plot(curve)
        plt.plot(curve + curve[::-1])
        plt.show()

    plot(1)
    plot(1.5)
    plot(2)
f()

def round80(a):
    i = np.rint(a * 0x80)
    a2 = i / 0x80
    return (i, a2)

def f():
    import matplotlib.pyplot as plt
    import numpy as np

    TWOPI = 2 * np.pi

    def plot(curve, name):
        [p] = plt.plot(curve, label=name)
        plt.plot(
            curve + curve[::-1],
            label=f"{name} plus reverse",
            color=p.get_color(),
            linestyle='dashed',
        )

    plot(pan ** 1.5, "pan^1.5")

    domain = np.linspace(-TWOPI / 4, TWOPI / 4, pan.size)
    curve = (1 + np.sin(domain)) / 2
    # plot(curve, "sin()")

    x = np.linspace(0, 1, pan.size)
    # smooth = 3*x**2 - 2*x**3
    # plot(smooth, "smoothstep")

    smoother = 6*x**5 - 15*x**4 + 10*x**3
    plot(smoother, "smootherstep")

    plt.legend()
    plt.show()

    plot(pan, "pan")
    root = curve ** (2/3)
    plot(root, "sin() ^ (2/3)")
    _, root_round = round80(root)
    plot(root_round, "sin() ^ (2/3), rounded")

    plt.legend()
    plt.show()
f()

def f():
    pan2 = np.interp(np.linspace(0, pan.size - 1, 33), np.arange(pan.size), pan)
    pan2i, pan2 = round80(pan2)
    print(repr(pan2i))

    def plot(curve):
        x = np.linspace(0, 1, curve.size)
        plt.plot(x, curve)
        plt.plot(x, curve + curve[::-1])

    plot(pan2)
    plt.title("interpolated")
    plt.show()

    plot(pan2 ** 1.5)
    plt.title("interpolated^1.5")
    plt.show()
f()

# !pip install ipympl (slow)
# from google.colab import output
# output.enable_custom_widget_manager()

# Manually-created 33-element pan curve designed to match the SMW pan curve as closely as possible.
# %matplotlib widget (this improperly collapses both show() calls into a single graph)
def f():
    pan2i = np.array([
          0,   1,   2,   3,   5,   8,  12,  16,
         21,  27,  33,  40,  47,  55,  63,  72,
         81,  89,  96, 102, 107, 111, 114, 117,
        119, 121, 122, 123, 124, 125, 126, 126,
        127
    ])

    print(', '.join(f"${x:02X}" for x in pan2i[::-1]))

    pan2 = pan2i / np.float32(0x80)

    def plot(curve, label):
        x = np.linspace(0, 1, curve.size)
        plt.plot(x, curve, label=label)
        plt.plot(x, curve + curve[::-1], label=label)

    plot(pan ** 1.5, "pan^1.5")
    plot(pan2 ** 1.5, "interpolated^1.5")
    plt.legend()
    plt.show()

    plot(pan, "pan")
    plot(pan2, "interpolated")
    plt.legend()
    plt.show()
f()
